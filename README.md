# Documentation

## How to contribute

Documentation for the GFX CI is stored in this respository as a collection of
["GitLab Flavored Markdown" (GFM)](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
files.

Please propose changes to them by creating merge requests. Discussions will
happen inside of them. 

Finally, make sure you set the notification settings (the bell icon in the
project details view) to "Watch" so as not to miss any new proposal.
